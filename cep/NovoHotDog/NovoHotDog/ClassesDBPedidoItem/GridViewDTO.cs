﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NovoHotDog.ClassesDBPedidoItem
{
    public class GridViewDTO
    {
        public string produto { get; set; }
        public decimal preco { get; set; }
        public int idproduto { get; set; }
        public string observacao { get; set; }
    }
}
