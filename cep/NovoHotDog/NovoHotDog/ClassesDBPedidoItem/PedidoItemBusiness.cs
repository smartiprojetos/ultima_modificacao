﻿using NovoHotDog.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NovoHotDog.ClassesDBPedidoItem
{
    public class PedidoItemBusiness
    {
        public int Salvar (PedidoItemDTO dto)
        {
            PedidoItemDataBase db = new PedidoItemDataBase();
            return db.Salvar(dto);
        }
    }
}
