﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NovoHotDog.ClassesDBPedidoItem
{
    public class PedidoItemDTO
    {

        public int id_pedido_item { get; set; }
        public int id_produto { get; set; }
        public int id_pedido { get; set; }
        public string Observacoes { get; set; }
    }
}
