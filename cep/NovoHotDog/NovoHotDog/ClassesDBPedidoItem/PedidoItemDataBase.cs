﻿using MySql.Data.MySqlClient;
using NovoHotDog.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NovoHotDog.ClassesDBPedidoItem
{
    public class PedidoItemDataBase
    {
        public int Salvar(PedidoItemDTO dto)
        {
            string script = @"insert into tb_pedido_item (id_produto,id_pedido,observacao)
                                              values (@id_produto,@id_pedido,@observacao)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.id_produto));
            parms.Add(new MySqlParameter("id_pedido", dto.id_pedido));          
            parms.Add(new MySqlParameter("observacao", dto.Observacoes));

            DataBase db = new DataBase();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;

        }


    }
}
