﻿using NovoHotDog.ClassesdoDB;
using NovoHotDog.Telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovoHotDog
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            VerificarPermissoes();
        }
        int pk = 0;
        string usu = string.Empty;
        public void CarregarDados (int Id, string Usuário)
        {
             pk = Id;
             usu = Usuário;
        }
        public void VerificarPermissoes()
        {
            if(UserSession.UsuarioLogado.bt_permissao_adm==false)
            {
                if(UserSession.UsuarioLogado.bt_permissao_produto==false)
                {
                    cadastrarProdutosToolStripMenuItem.Enabled =false;
                }
                if(UserSession.UsuarioLogado.bt_permissao_pedido==false)
                {
                    rEalizarPedidosToolStripMenuItem.Enabled = false;
                }
                if(UserSession.UsuarioLogado.bt_permissao_cadastrar==false)
                {
                    novoCadastroToolStripMenuItem.Enabled =false;
                }
            }
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoginFuncionario login = new LoginFuncionario();
            login.Show();
            Form1 form = new Form1();
            form.Hide();
        }

        private void novoCadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastroFuncionario cadastrar = new CadastroFuncionario();
            cadastrar.Show();
           
        }

        private void label2_Click(object sender, EventArgs e)
        {
            DialogResult re = MessageBox.Show("Deseja mesmo sair?",
                                "Sair",
                                MessageBoxButtons.OKCancel,
                                MessageBoxIcon.Question);
            if (re == DialogResult.OK)
            {
                Application.Exit();
            }


        }

        private void rEalizarPedidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            realizarPedido fzpedido = new realizarPedido();
            fzpedido.LoadScreen(pk, usu);
            fzpedido.Show();
            
        }

        private void consultarPedidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarPedido consultar = new ConsultarPedido();
            consultar.Show();
        }

        private void cadastrarProdutosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cadastro_Produto cadastro_Produto = new Cadastro_Produto();
            cadastro_Produto.Show();
        }

        private void loginFuncionárioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.WindowState=FormWindowState.Minimized;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
