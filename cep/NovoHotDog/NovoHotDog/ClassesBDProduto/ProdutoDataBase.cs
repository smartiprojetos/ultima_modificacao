﻿using MySql.Data.MySqlClient;
using NovoHotDog.DB;
using System.Collections.Generic;

namespace NovoHotDog.ClassesBDProduto
{
    public class ProdutoDataBase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"insert into tb_produto (nm_produto, vl_preco) values (@nm_produto,@vl_preco)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Produto));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));
            DataBase db = new DataBase();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<ProdutoDTO> Listar()
        {
            string script = @"select * from tb_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ProdutoDTO> produtos = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Produto = reader.GetString("nm_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                produtos.Add(dto);
            }
            return produtos;


        }
       
    }
}
