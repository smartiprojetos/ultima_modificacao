﻿using System;
using System.Collections.Generic;

namespace NovoHotDog.ClassesBDProduto
{
    public class ProdutoBusiness
    {
        public int Salvar (ProdutoDTO dto)
        {
            if(dto.Produto==string.Empty)
            {
                throw new ArgumentException("Produto é obrigátório.");
            }
            if (dto.Preco == 0)
            {
                throw new ArgumentException("Preco é obrigatório.");
            }
            ProdutoDataBase db = new ProdutoDataBase();
            int pk =db.Salvar(dto);
            return pk;
        }
        public List<ProdutoDTO> Listar()
        {
            ProdutoDataBase db = new ProdutoDataBase();
            return db.Listar();
        }
    }
}
