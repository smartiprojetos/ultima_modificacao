﻿using MySql.Data.MySqlClient;
using NovoHotDog.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NovoHotDog.ClassesdoDB
{
    public class FuncionarioDataBase
    {
        public int Salvar(FuncionarioDTO dto)
        {
            string script = @"INSERT into tb_funcionario
         (
            nm_funcionario,
            cpf, nm_funcao,
            Login,senha,
            dt_horario,
            bt_permissao_adm,
            bt_permissao_produto,
            bt_permissao_pedido,
            bt_permissao_cadastrar,
            ds_cep,
            ds_estado,
            ds_cidade,
            ds_rua,
            ds_bairro
            )  
          VALUES
            ( 
            @nm_funcionario,
            @cpf,@nm_funcao,
            @Login, 
            @senha,
            @dt_horario,
            @bt_permissao_adm,
            @bt_permissao_produto,
            @bt_permissao_pedido,
            @bt_permissao_cadastrar
            @ds_cep,
            @ds_estado,
            @ds_cidade,
            @ds_rua,
            @ ds_bairro
             )";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.nm_completo));
            parms.Add(new MySqlParameter("cpf", dto.CPF));
            parms.Add(new MySqlParameter("nm_funcao", dto.Cargo));
            parms.Add(new MySqlParameter("Login", dto.nm_usuario));
            parms.Add(new MySqlParameter("senha", dto.senha));
            parms.Add(new MySqlParameter("dt_horario", dto.Data));
            parms.Add(new MySqlParameter("bt_permissao_adm", dto.bt_permissao_adm));
            parms.Add(new MySqlParameter("bt_permissao_produto", dto.bt_permissao_produto));
            parms.Add(new MySqlParameter("bt_permissao_pedido", dto.bt_permissao_pedido));
            parms.Add(new MySqlParameter("bt_permissao_cadastrar", dto.bt_permissao_cadastrar));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));
            parms.Add(new MySqlParameter("ds_estado", dto.Estado));
            parms.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("ds_rua", dto.Rua));
            parms.Add(new MySqlParameter("ds_bairro", dto.Bairro));






            DataBase db = new DataBase();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public bool Consultar(string usuario, string senha)
        {
            string script = @"select * from tb_funcionario where
                              Login = @Login and
                              senha = @senha";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Login", usuario));
            parms.Add(new MySqlParameter("senha", senha));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public List<FuncionarioDTO> Listar()
        {
            string script = @"select * from tb_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id = reader.GetInt32("id_funcionario");
                dto.nm_completo = reader.GetString("nm_funcionario");
                dto.Cargo = reader.GetString("nm_funcao");
                dto.CPF = reader.GetString("cpf");
                dto.Data = reader.GetDateTime("dt_horario");
                dto.nm_usuario = reader.GetString("Login");
                dto.senha = reader.GetString("senha");
                lista.Add(dto);
            }
            reader.Close();
            return lista;

        }

        public FuncionarioDTO Login(string usuario, string senha)
        {
            string script = @"select * from tb_funcionario where
                              Login = @Login and
                              senha = @senha";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Login", usuario));
            parms.Add(new MySqlParameter("senha", senha));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO funcionario = null;

            if(reader.Read())
            {
                funcionario = new FuncionarioDTO();
                funcionario.Id = reader.GetInt32("id_funcionario");
                funcionario.nm_completo = reader.GetString("nm_funcionario");
                funcionario.Cargo = reader.GetString("nm_funcao");
                funcionario.CPF = reader.GetString("cpf");
                funcionario.Data = reader.GetDateTime("dt_horario");
                funcionario.nm_usuario = reader.GetString("Login");
                funcionario.senha = reader.GetString("senha");
                funcionario.bt_permissao_adm = reader.GetBoolean("bt_permissao_adm");
                funcionario.bt_permissao_produto = reader.GetBoolean("bt_permissao_produto");
                funcionario.bt_permissao_pedido = reader.GetBoolean("bt_permissao_pedido");
                funcionario.bt_permissao_cadastrar = reader.GetBoolean("bt_permissao_cadastrar");

            }
            reader.Close();
            return funcionario;
        }
    }
}
