﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NovoHotDog.ClassesdoDB
{
    public class FuncionarioDTO
    {
        public int Id { get; set; }
        public string nm_completo { get; set; }
        public string CPF { get; set; }
        public string Cargo { get; set; }
        public string nm_usuario { get; set; }
        public string senha { get; set; }
        public DateTime Data { get; set; }
        public bool bt_permissao_adm { get; set; }
        public bool bt_permissao_produto{ get; set; }
        public bool bt_permissao_pedido{ get; set; }
        public bool bt_permissao_cadastrar { get; set; }



        //cep
        public int Cep { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string Rua { get; set; }
        public string Bairro { get; set; }
    }
}
