﻿using NovoHotDog.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NovoHotDog.ClassesdoDB
{
    public class FuncionarioBusiness
    {
        public int Salvar (FuncionarioDTO dto)
        {

            FuncionarioDataBase db = new FuncionarioDataBase();
            int pk = db.Salvar(dto);
            return pk;
        }

        FuncionarioDataBase db = new FuncionarioDataBase();
        public bool Logar (string usuario,string senha)
        {
            bool logou = db.Consultar(usuario, senha);
            return logou;
        }

        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDataBase db = new FuncionarioDataBase();
            return db.Listar();
        }

        public FuncionarioDTO Login(string usuario,string senha)
        {
            if(usuario==string.Empty)
            {
                throw new ArgumentException("Usuário é obrigatório.");
            }

            if (senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatório");
            }
            FuncionarioDataBase db = new FuncionarioDataBase();
            return db.Login(usuario, senha);
        }
    }
}
