﻿using NovoHotDog.ClassesBDProduto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovoHotDog.Telas
{
    public partial class Cadastro_Produto : Form
    {
        public Cadastro_Produto()
        {
            InitializeComponent();
        }
     

        private void label8_Click(object sender, EventArgs e)
        {

            Form1 tela = new Form1();
            tela.Show();
            this.Close();

            //DialogResult re = MessageBox.Show("Deseja mesmo sair?",
            //                    "Sair",
            //                    MessageBoxButtons.OKCancel,
            //                    MessageBoxIcon.Question);
            //if (re == DialogResult.OK)
            //{
            //    Close();
            //}
            //Form1 voltaraomenu = new Form1();
            //voltaraomenu.Show();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                decimal x = Convert.ToDecimal(txtpreco.Text);
                if (x < 0)
                {
                    MessageBox.Show("Por favor não coloque valores negativos",
                                         "Aviso",
                                         MessageBoxButtons.OK,
                                         MessageBoxIcon.Information);
                }
                else
                {
                    if (txtpreco.Text.Trim() == string.Empty
                    || txtproduto.Text.Trim() == string.Empty)
                    {
                        MessageBox.Show("Por favor não coloque só espaços",
                                             "Aviso",
                                             MessageBoxButtons.OK,
                                             MessageBoxIcon.Information);
                    }
                    if (txtproduto.Text == string.Empty)
                    {
                        MessageBox.Show("Não esqueça de preencher o nome do produto.", 
                                        "Aviso",
                                        MessageBoxButtons.OK, 
                                        MessageBoxIcon.Information);
                    }

                    else
                    {
                        ProdutoDTO dto = new ProdutoDTO();
                        dto.Produto = txtproduto.Text;
                        dto.Preco = Convert.ToDecimal(txtpreco.Text);
                        ProdutoBusiness business = new ProdutoBusiness();
                        business.Salvar(dto);
                        MessageBox.Show("Produto cadastrado com sucesso.", 
                                        "Cadastro",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                    }
                }

    }
            catch (Exception ex)
            {
                MessageBox.Show("Por favor insira apenas números no campo preço.", 
                                "Erro", 
                                MessageBoxButtons.OK, 
                                MessageBoxIcon.Error);
            }
            
        }

       
        private void button1_Click_1(object sender, EventArgs e)
        {
            txtproduto.Text = string.Empty;
            txtpreco.Text = string.Empty;
            Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtproduto_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtpreco_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtpreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char.IsDigit(e.KeyChar) == true || (char.IsControl(e.KeyChar) == true)|| (char.IsPunctuation(e.KeyChar))))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtpreco_Click(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtpreco, "Digite o preço unitario do produto.");
            toolTip1.IsBalloon = true;
            toolTip1.ToolTipIcon = ToolTipIcon.Info;
            toolTip1.ToolTipTitle = "";
        }
    }
}
