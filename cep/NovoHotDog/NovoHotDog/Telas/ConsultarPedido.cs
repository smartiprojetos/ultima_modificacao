﻿using NovoHotDog.ClassesDBPedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovoHotDog.Telas
{
    public partial class ConsultarPedido : Form
    {
        public ConsultarPedido()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            //Form1 tela = new Form1();
            //tela.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void dgvconsultarpedido_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            PedidoBusiness business = new PedidoBusiness();
            dgvconsultarpedido.AutoGenerateColumns = false;
            dgvconsultarpedido.DataSource = business.Consultar(txtCliente.Text);
        }

        private void dgvconsultarpedido_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtCliente_Click(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtCliente, "Digite o nome do cliente");
            toolTip1.IsBalloon = true;
            toolTip1.ToolTipIcon = ToolTipIcon.Info;
            toolTip1.ToolTipTitle = "";
        }
    }
}
