﻿using Correios;
using NovoHotDog.ClassesdoDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovoHotDog.Telas
{
    public partial class CadastroFuncionario : Form
    {
        public CadastroFuncionario()
        {
            InitializeComponent();
        }

        Regex cpf = new Regex(@"^\d{3}\.\d{3}\.\d{3}-\d{2}$");

        private void label5_Click(object sender, EventArgs e)
        {
            //Form1 tela = new Form1();
            //tela.Show();
            this.Close();
        }

        private void CadastroFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtnome.Text.Trim() == string.Empty
                                || txtcpf.Text.Trim() == string.Empty
                                || txtcargo.Text.Trim() == string.Empty
                                || txtconfirmesenha.Text.Trim() == string.Empty
                                || txtsenha.Text.Trim() == string.Empty
                                || txtusuario.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("Por favor não coloque só espaços",
                                         "Aviso",
                                         MessageBoxButtons.OK,
                                         MessageBoxIcon.Information);
                }

                if (txtnome.Text == string.Empty || txtcargo.Text == string.Empty || txtusuario.Text == string.Empty || txtusuario.Text == string.Empty)
                {
                    MessageBox.Show("Por favor não se esqueça de preencher nenhum campo.",
                                    "Aviso",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                if (txtsenha.Text != txtconfirmesenha.Text)
                {
                    MessageBox.Show("Senhas não correspondem",
                                    "Aviso",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                else
                {
                    FuncionarioDTO dto = new FuncionarioDTO();
                    bool validou = cpf.IsMatch(txtcpf.Text);
                    if (validou == true)
                    {
                        dto.nm_completo = txtnome.Text;
                        dto.CPF = txtcpf.Text;
                        dto.Cargo = txtcargo.Text;
                        dto.nm_usuario = txtusuario.Text;
                        dto.bt_permissao_adm = chkADM.Checked;
                        dto.bt_permissao_produto = chkProdutos.Checked;
                        dto.bt_permissao_pedido = chkPedidos.Checked;
                        dto.bt_permissao_cadastrar = chkfuncionarios.Checked;

                        dto.Cep = Convert.ToInt32(txtcep.Text);
                        dto.Estado = txtestado.Text;
                        dto.Cidade = txtcidade.Text;
                        dto.Rua = txtrua.Text;

                        SHA256Cript crip = new SHA256Cript();
                        dto.senha = crip.Criptografar(txtsenha.Text);
                        dto.Data = DateTime.Now;


                        FuncionarioBusiness business = new FuncionarioBusiness();
                        business.Salvar(dto);

                        MessageBox.Show("Dados salvos", 
                                        "Cadastrar", 
                                        MessageBoxButtons.OK, 
                                        MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Digite seu CPF e não esqueça dos pontos e traço.",
                                        "Aviso",
                                         MessageBoxButtons.OK, 
                                         MessageBoxIcon.Information);
                    }
                }

            
            }
            catch (Exception)
            {

                MessageBox.Show("Deu erro , confira tudo , talvez seu CPF já tenha sido cadastrado",
                                  "Erro",
                                  MessageBoxButtons.OK, 
                                  MessageBoxIcon.Error);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
          
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            txtnome.Text = string.Empty;
            txtcpf.Text = string.Empty;
            txtcargo.Text = string.Empty;
            txtcpf.Text = string.Empty;
            txtusuario.Text = string.Empty;
            txtsenha.Text = string.Empty;
            Close();
        }


        private void txtcpf_TextChanged(object sender, EventArgs e)
        {
            bool validou = cpf.IsMatch(txtcpf.Text);
            if (validou == true)
            {
                txtcpf.BackColor = Color.LawnGreen;
            }
            else
            {
                txtcpf.BackColor = Color.IndianRed;
            }
        }

        private void txtnome_Click(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtnome, "Digite o nome completo ");
            toolTip1.IsBalloon = true;
            toolTip1.ToolTipIcon = ToolTipIcon.Info;
            toolTip1.ToolTipTitle = "";
        }

        private void txtconfirmesenha_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtcep_Leave(object sender, EventArgs e)
        {
            
            
        }

        private void txtcep_Click(object sender, EventArgs e)
        {
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
          
          
            
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            try
            {
                CorreiosApi service = new CorreiosApi();
                var cep = txtcep.Text;
                var dados = service.consultaCEP(cep);
                txtbairro.Text = dados.bairro;
                txtestado.Text = dados.uf;
                txtcidade.Text = dados.cidade;
                txtrua.Text = dados.end;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Algo está errado com o seu CEP.",
                                "Erro",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
    }
}
