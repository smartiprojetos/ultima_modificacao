﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovoHotDog.Telas
{
    public partial class frmIniciar : Form
    {
        public frmIniciar()
        {
            InitializeComponent();
          
            Task.Factory.StartNew(() =>
            {
               
                System.Threading.Thread.Sleep(5000);

                Invoke(new Action(() =>
                {
                    
                    frmTeladeRedirecionamento frm = new frmTeladeRedirecionamento();
                    frm.Show();
                    Hide();
                }));
            });
        }

        private void frmIniciar_Load(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
