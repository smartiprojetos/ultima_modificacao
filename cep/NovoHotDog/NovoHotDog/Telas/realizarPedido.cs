﻿using NovoHotDog.ClassesBDProduto;
using NovoHotDog.ClassesDBPedido;
using NovoHotDog.ClassesDBPedidoItem;
using NovoHotDog.ClassesdoDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace NovoHotDog.Telas
{
    public partial class realizarPedido : Form
    {
       
        public realizarPedido()
        {
            InitializeComponent();
            CarregarCombo();
            dgvteste.AutoGenerateColumns = false;
            dgvteste.DataSource = listadepedido;
        }

        int pkfunci = 0;
        Regex cpf = new Regex(@"^\d{3}\.\d{3}\.\d{3}-\d{2}$");
        Regex validarobservacao = new Regex(@"^[ a-zA-Z á]*$");
        Regex validarcliente = new Regex(@"^[ a-zA-Z á]*$");
        BindingList<GridViewDTO> listadepedido = new BindingList<GridViewDTO>();
        FuncionarioDTO funcionario = new FuncionarioDTO();
        public void LoadScreen (int Id, string usuario)
        {
            lblnmusuario.Text = usuario;
            pkfunci = Id; 
           
        }
       
        private void realizarPedido_Load(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
            //Form1 tela = new Form1();
            //tela.Show();
            this.Close();

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {           
        }
        
        public void CarregarCombo()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> produto = business.Listar();
            cboProdutos.DisplayMember = nameof(ProdutoDTO.Produto);
            cboProdutos.ValueMember = nameof(ProdutoDTO.Id);
            cboProdutos.DataSource = produto;
        }

      
        void button3_Click(object sender, EventArgs e)
        {
            if (dupquantidade.Value == 0 || cboProdutos.Text == string.Empty)
            {
                MessageBox.Show("Preencha os campos produto e quantidade antes de adicionar o pedido.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                
                decimal quantidade = dupquantidade.Value;
                ProdutoDTO dto = cboProdutos.SelectedItem as ProdutoDTO;
                bool validar = validarobservacao.IsMatch(txtobservacao.Text);

                if (quantidade == 0 )
                {
                    MessageBox.Show(" Não existe 0 em quantidade", "Atenção",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
                else
                {

                
                if (validar == true)
            {
                for (int i = 0; i < quantidade; i++)
                {
                        GridViewDTO itens = new GridViewDTO();
                        itens.idproduto =dto.Id;
                        itens.preco = dto.Preco;
                        itens.produto = dto.Produto;
                        itens.observacao = txtobservacao.Text;
                        listadepedido.Add(itens);
                }
                 
               
                txtobservacao.Text = string.Empty;
            }
                
                else
                { 
                MessageBox.Show("O campo Observação aceita apenas letras.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                }

            }
}

        private void button2_Click_1(object sender, EventArgs e)
        {
            if(txtcliente.Text==string.Empty || cbopagamento.Text==string.Empty)
            {
               MessageBox.Show("Preencha o campo nome do cliente e forma de pagamento antes de prosseguir.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                bool validou = cpf.IsMatch(txtcpf.Text);
                if (validou == true)
                {
                  bool validar = validarcliente.IsMatch(txtcliente.Text);
                          if(validar==true)
                          {
                            PedidoDTO dados = new PedidoDTO();
                            dados.IdFuncionario = pkfunci;
                            dados.Cliente = txtcliente.Text;
                            dados.CPF = txtcpf.Text;
                            dados.Data = DateTime.Now;
                            dados.Quantidade = dupquantidade.Value;
                            dados.Pagamento = cbopagamento.SelectedItem.ToString();
                            PedidoBusiness bussines1 = new PedidoBusiness();
                            bussines1.Salvar(dados, listadepedido.ToList());
                      
                            MessageBox.Show("Seu pedido foi realizado com sucesso.", "Pedido", MessageBoxButtons.OK, MessageBoxIcon.Information);
                          }
                          else
                          {
                            MessageBox.Show("O campo cliente aceita apenas letras.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                          }
                }
                else
                {
                    MessageBox.Show("Digite seu CPF e não esqueça dos pontos e traço.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }


           
            
        }

        private void dgvtabela_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvteste_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtcpf_TextChanged(object sender, EventArgs e)
        {
            bool validou = cpf.IsMatch(txtcpf.Text);
            if (validou == true)
            {
                txtcpf.BackColor = Color.LawnGreen;
            }
            else
            {
                txtcpf.BackColor = Color.IndianRed;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtcliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char.IsLetter(e.KeyChar) == true || (char.IsControl(e.KeyChar)==true) || char.IsSeparator(e.KeyChar) == true))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
