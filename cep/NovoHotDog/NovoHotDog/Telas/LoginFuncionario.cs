﻿using NovoHotDog.ClassesdoDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovoHotDog.Telas
{
    public partial class LoginFuncionario : Form
    {
        public LoginFuncionario()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            //Form1 voltaaomenu = new Form1();
            //voltaaomenu.Show();
            Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            try
            { 
            if (txtsenha.Text.Trim() == string.Empty
                || txtusuario.Text.Trim() == string.Empty
               )
            {
                MessageBox.Show("Por favor não coloque só espaços",
                                     "Aviso",
                                     MessageBoxButtons.OK,
                                     MessageBoxIcon.Information);
            }
            FuncionarioBusiness business = new FuncionarioBusiness();
            
            SHA256Cript crip = new SHA256Cript();
            string senha = crip.Criptografar(txtsenha.Text);
            FuncionarioDTO funcionario = business.Login(txtusuario.Text, senha);
            FuncionarioDTO dto = new FuncionarioDTO();
            int id =dto.Id = funcionario.Id;
            string usuario =dto.nm_usuario = funcionario.nm_usuario;
            
           
            if(funcionario!=null)
            {
                UserSession.UsuarioLogado = funcionario;
                Form1 form = new Form1();
                form.CarregarDados(id, usuario);
                form.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Dados inválidos",
                                "Erro",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            }
            catch
            {
                MessageBox.Show("Por favor insira todos os dados corretamente", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            //bool logou = business.Logar(txtusuario.Text, senha);
            //if (logou == true)
            //{


            //   DialogResult re = MessageBox.Show("Login efetuado com sucesso", 
            //                                        "Login",
            //                                        MessageBoxButtons.OK, 
            //                                        MessageBoxIcon.Information);
            //    if(re==DialogResult.OK)
            //    {
            //        Form1 form = new Form1();
            //        form.Show();
            //        Close();
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("Dados inválidos",
            //                    "Erro",
            //                    MessageBoxButtons.OK, 
            //                    MessageBoxIcon.Error);
            //}
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            txtusuario.Text = string.Empty;
            txtsenha.Text = string.Empty;
            Close();
        }

        private void LoginFuncionario_Load(object sender, EventArgs e)
        {

        }


        private void txtsenha_Click(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtsenha, "Caracteres maiusculos e minusculos são considerados!");
            toolTip1.IsBalloon = true;
            toolTip1.ToolTipIcon = ToolTipIcon.Info;
            toolTip1.ToolTipTitle = "";
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtsenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtsenha.MaxLength != 10)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
