﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovoHotDog.Telas
{
    public partial class frmTeladeRedirecionamento : Form
    {
        public frmTeladeRedirecionamento()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            DialogResult re = MessageBox.Show("Deseja mesmo sair?",
                                "Sair",
                                MessageBoxButtons.OKCancel,
                                MessageBoxIcon.Question);
            if (re == DialogResult.OK)
            {
                Close();
            }
            Form1 voltaraomenu = new Form1();
            voltaraomenu.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoginFuncionario nha = new LoginFuncionario();
            nha.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           frmTeladoADM oi = new frmTeladoADM();
            oi.Show();
            Hide();
        }

        private void frmTeladeRedirecionamento_Load(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
