﻿using NovoHotDog.ClassesdoDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovoHotDog.Telas
{
    public partial class frmTeladoADM : Form
    {
        public frmTeladoADM()
        {
            InitializeComponent();
        }

        private void txtnha_Click(object sender, EventArgs e)
        {
            if (txtsenhaadm.Text.Trim() == string.Empty
                || txtusuarioadm.Text.Trim() == string.Empty
                    )
            {
                MessageBox.Show("Por favor não coloque só espaços",
                                     "Aviso",
                                     MessageBoxButtons.OK,
                                     MessageBoxIcon.Information);
            }
            FuncionarioBusiness business = new FuncionarioBusiness();

            SHA256Cript crip = new SHA256Cript();
            string senha = crip.Criptografar(txtsenhaadm.Text);
            FuncionarioDTO funcionario = business.Login(txtusuarioadm.Text, senha);
            if (funcionario != null)
            {
                UserSession.UsuarioLogado = funcionario;
                Form1 form = new Form1();
                form.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Dados inválidos",
                                "Erro",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

        }

        private void label2_Click(object sender, EventArgs e)
        {
            DialogResult re =  MessageBox.Show("Deseja mesmo sair?",
                                "Sair",
                                MessageBoxButtons.OKCancel,
                                MessageBoxIcon.Question);
            if (re == DialogResult.OK)
            {
                Application.Exit();
            }


          
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtsenhaadm_Click(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtsenhaadm, "Caracteres maiusculos e minusculos são considerados!");
            toolTip1.IsBalloon = true;
            toolTip1.ToolTipIcon = ToolTipIcon.Info;
            toolTip1.ToolTipTitle = "";
        }

        private void txtsenhaadm_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (char.IsLetter(e.KeyChar) == true || (char.IsControl(e.KeyChar) == true))
            //{
            //    e.Handled = false;
            //}
            //else
            //{
            //    e.Handled = true;
            //}
        }

        private void txtsenhaadm_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click_1(object sender, EventArgs e)
        {
            Close();
        }
    }
}
