﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NovoHotDog.ClassesDBPedido
{
    public class PedidoDTO
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public string Cliente { get; set; }
        public decimal Quantidade { get; set; }
        public Decimal Valor { get; set; }
        public string CPF { get; set; }
        public string Pagamento { get; set; }
        public int IdFuncionario { get; set; }
    }
}
