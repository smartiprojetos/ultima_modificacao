﻿using NovoHotDog.ClassesBDProduto;
using NovoHotDog.ClassesDBPedidoItem;
using System.Collections.Generic;

namespace NovoHotDog.ClassesDBPedido
{
    public class PedidoBusiness
    {
        public int Salvar(PedidoDTO idpedido, List<GridViewDTO> itensdopedido)
        {
            PedidoDataBase db = new PedidoDataBase();
            int pk = db.Salvar(idpedido);

            PedidoItemBusiness busi = new PedidoItemBusiness();
            foreach (GridViewDTO item in itensdopedido)
            {
                PedidoItemDTO dto = new PedidoItemDTO();
                dto.id_produto = item.idproduto;
                dto.id_pedido = pk;
                dto.Observacoes = item.observacao;
                busi.Salvar(dto);
            }
            return pk;
        }

        public List<ConsultarPedidoDTO> Consultar(string Cliente)
        {
            PedidoDataBase db = new PedidoDataBase();
            return db.Consultar(Cliente);
            
        }
    }
}
