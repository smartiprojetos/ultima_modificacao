﻿using MySql.Data.MySqlClient;
using NovoHotDog.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NovoHotDog.ClassesDBPedido
{
    public class PedidoDataBase
    {
        public int Salvar(PedidoDTO dto)
        {
            string script = @"INSERT
                                INTO tb_pedido 
                                (
                                 dt_venda,
                                 nm_clinte,
                                 qt_quantidade,
                                 ds_cpf,
                                 fm_pagamento,
                                 id_funcionario
                                 )
                                 VALUES
                                (
                                @dt_venda,
                                @nm_clinte,
                                @qt_quantidade,
                                @ds_cpf,
                                @fm_pagamento,
                                @id_funcionario
                                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_venda", dto.Data));
            parms.Add(new MySqlParameter("nm_clinte", dto.Cliente));
            parms.Add(new MySqlParameter("qt_quantidade", dto.Quantidade));         
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));           
            parms.Add(new MySqlParameter("fm_pagamento", dto.Pagamento));
            parms.Add(new MySqlParameter("id_funcionario", dto.IdFuncionario));
            DataBase db = new DataBase();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

       public List<ConsultarPedidoDTO> Consultar(string cliente)
        {
            string script = @"SELECT * FROM consulta_pedido WHERE nm_clinte like @nm_clinte";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_clinte", cliente + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script,parms);
            List<ConsultarPedidoDTO> lista = new List<ConsultarPedidoDTO>();
            while (reader.Read())
            {
                ConsultarPedidoDTO dto = new ConsultarPedidoDTO();
                dto.Id = reader.GetInt32("id_pedido");
                dto.Cliente = reader.GetString("nm_clinte");
                dto.Data = reader.GetDateTime("dt_venda");
                dto.Produto = reader.GetString("nm_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                dto.Observacoes = reader.GetString("observacao");
                
                lista.Add(dto);
            }
            reader.Close();
           return lista;
        }

        
    }
}
